import React from 'react';
import NewsList from '../Widgets/NewsList/newsList';
import NewsSlider from '../Widgets/NewsSlider/slider';
import VideoList from '../Widgets/VideoList/videolist';

const Home = () => (
    <div>
        <NewsSlider 
            type="featured"
            start={0}
            amount={4}
            settings={{
                dots: false
            }}
            />
        <NewsList 
            type="card"
            loadmore={true}
            start={4}
            amount={3}
            />
        <VideoList 
            type="card"
            title={true}
            loadmore={false}
            start={0}
            amount={3}
            />
    </div>
)

export default Home