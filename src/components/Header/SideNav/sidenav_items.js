import React from 'react'
import FontAwesome from 'react-fontawesome'
import { Link } from 'react-router-dom'
import styles from './sideNav.module.css'

const SideNavItems = () => {

    const items = [
        {
            type: styles.option,
            icon: 'home',
            text: 'Home',
            link: '/'
        },
        {
            type: styles.option,
            icon: 'file-text-o',
            text: 'News',
            link: '/news'
        },
        {
            type: styles.option,
            icon: 'play',
            text: 'Videos',
            link: '/videos'
        },
        {
            type: styles.option,
            icon: 'sign-in',
            text: 'Sign in',
            link: '/signin'
        },
        {
            type: styles.option,
            icon: 'sign-out',
            text: 'Sign out',
            link: '/signout'
        }
    ]

    const showNavItems = () => {
        return items.map((item, i) => {
            return (
                <div key={i} className={item.type}>
                    <Link to={item.link}>
                        <FontAwesome name={item.icon}/>
                        {item.text}
                    </Link>
                </div>
            )
        })
    }

    return (
        <div>
            {showNavItems()}
        </div>
    )
}

export default SideNavItems

