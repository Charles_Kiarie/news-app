import React from 'react';
import styles from './header.module.css'
import { Link } from 'react-router-dom';
import FontAwesome from 'react-fontawesome';
import SideNavigation from './SideNav/sidenavigation';

const Header = (props) => {

    const navBars = () => (
        <div className={styles.bars}>
            <FontAwesome name="bars"
            onClick={props.onShowNav}
                style={{
                    color:'#dfdfdf',
                    padding:'10px',
                    cursor:'pointer'
                }}
            />
        </div>
    )

    const logo = () => (
        <Link to="/" className={styles.logo}>
            <img alt="nba logo" src="/images/nba_logo.png"/>
        </Link>
    )

    return (
        <header className={styles.header}>
            <SideNavigation {...props}/>
            <div className={styles.headerOpt}>
                {navBars()}
                {logo()}
            </div>
        </header>
    )
}

export default Header;