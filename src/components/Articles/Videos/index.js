import React, {Component} from 'react';
import axios from 'axios';
import { URL } from '../../../helper';
import Header from './header';

import styles from '../articles.module.css'
import VideosRelated from '../../Widgets/VideoList/VideosRelated/videosrelated';


class VideoArticle extends Component {

    state = {
        article:[],
        team:[],
        teams: [],
        related: []
    }

    async componentWillMount() {
        let res = await axios.get(`${URL}/videos?id=${this.props.match.params.id}`)
        let article = res.data[0]

        let response = await axios.get(`${URL}/teams?id=${article.team}`)
        this.setState({
            article,
            team: response.data
        })
        this.getRelated();
    }

    getRelated = async () => {
        let res = await axios.get(`${URL}/teams`)
        let teams = res.data

        let response = await axios.get(`${URL}/videos?q=${this.state.team[0].city}&_limit=3`)
        this.setState({
            teams,
            related: response.data
        })
    }

    render() {
        const article = this.state.article
        const team = this.state.team
        console.log(this.state.related)
        return (
            <div>
                <Header teamData={team[0]} />
                <div className={styles.videowrapper}>
                    <h1>{article.title}</h1>
                    <iframe
                        title="videoplayer"
                        width="100%"
                        height="300px"
                        src={`https:\\www.youtube.com/embed/${article.url}`}
                    >
                    </iframe>
                </div>
                <VideosRelated data={this.state.related} teams={this.state.teams}/>
            </div>
        )
    }
}

export default VideoArticle