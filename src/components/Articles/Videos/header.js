import React from 'react'
import PostData from '../Elements/postdata';
import TeamInfo from '../Elements/teaminfo';

const Header = (props) => {
    
    const renderTeamInfo = (team) => {
        console.log(team)
        return team ? 
            <TeamInfo team={team} />  : null; 
    }

    // const renderPostData = (date, author) => (
    //     <PostData data={{date, author}}/>
    // )

    return (
        <div>
            {renderTeamInfo(props.teamData)}
            {/* {renderPostData(props.date, props.author)} */}
        </div>
    )
}

export default Header