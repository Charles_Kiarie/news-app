import React, {Component} from 'react';
import axios from 'axios';
import { URL } from '../../../helper';
import Header from './header';

import styles from '../articles.module.css'


class NewsArticle extends Component {
    state = {
        article: [],
        team: []      
    }

    async componentWillMount() {
        let res = await axios.get(`${URL}/articles?id=${this.props.match.params.id}`)
        let article = res.data[0]

        let response = await axios.get(`${URL}/teams?id=${article.team}`)
        this.setState({
            article,
            team: response.data
        })
    }

    render() {
        const article = this.state.article
        const team = this.state.team

        return (
            <div className={styles.articlewrapper }>
                <Header teamData={team[0]} date={article.date} author={article.author}/>
                <div className={styles.articlebody}>
                    <h1>{article.title}</h1>
                    <div className={styles.articleimage}
                        style = {{
                            background: `url('/images/articles/${article.image}')`
                        }}
                    ></div>
                    <div className={styles.articletext}>{article.body}</div>
                </div>
            </div>
        )
    }
}

export default NewsArticle