import React from 'react'
import NewsList from '../../Widgets/NewsList/newsList'
import NewsSlider from '../../Widgets/NewsSlider/slider'

const NewsMain = () => {
    return (
        <div>
            <NewsSlider
                type="featured"
                settings={{dots:false}}
                start={0}
                amount={3}
            />
            <NewsList
                type="main"
                loadMore={true}
                start={3}
                amount={10}
            />
        </div>
    )
}

export default NewsMain