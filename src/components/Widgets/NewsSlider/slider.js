import axios from 'axios'
import React, {Component} from 'react'
import { URL } from '../../../helper'
import SliderTemplates from './slider_templates'

class NewsSlider extends Component {

    state = {
        news: []
    }

    async componentWillMount() {
        let res = await axios.get(`${URL}/articles?_start=${this.props.start}&_end=${this.props.amount}`)
        this.setState({
            news: res.data
        })
    }

    render() {
        return (
            <div>
                <SliderTemplates data={this.state.news} type={this.props.type} settings={this.props.settings}/>
            </div>
        )
    }
}

export default NewsSlider