import React, {Component} from 'react'
import Button from '../Button/button'
import axios from 'axios'

import styles from './videolist.module.css'
import VideoListTemplate from './videoListTemplate'
import { URL } from '../../../helper'

class VideoList extends Component {

    state = {
        videos: [],
        teams: [],
        start: this.props.start,
        end: this.props.start + this.props.amount,
        amount: this.props.amount
    }

    componentWillMount() {
        this.request(this.state.start, this.state.end)
    }

    request = async (start, end) => {
        if(this.state.teams < 1) {
            let res = await axios.get(`${URL}/teams`)
            this.setState({
               teams: res.data
            })
        }

        let res = await axios.get(`${URL}/videos?_start=${start}&_end=${end}`)
        this.setState({
            videos: [...this.state.videos, ...res.data],
            start,
            end
        })
    }

    renderTitle = () => {
        return this.props.title ? 
        <h3><strong>NBA</strong> Video</h3> : null
    }

    loadMore = () => {
        let end = this.state.end + this.state.amount
        this.request(this.state.end, end)
    }

    renderButton = () => {
        return this.props.loadmore ? 
        <Button type="loadmore" text="Load More Videos" loadmore={this.loadMore()}/> 
        : 
        <Button type="linkto" text="More Videos" linkTo="/videos"/>
    }

    renderVideos = () => {
        let template = null

        switch (this.props.type) {
            case 'card':
                template = (
                    <div>
                        <VideoListTemplate data={this.state.videos} teams={this.state.teams}/>
                    </div>
                )
                break;
        
            default:
                template = null
        }

        return template
    }

    render() {
        return (
            <div className={styles.videolist_wrapper}>
                {this.renderTitle()}
                {this.renderVideos()}
                {this.renderButton()}
            </div>
        )
    }
}

export default VideoList