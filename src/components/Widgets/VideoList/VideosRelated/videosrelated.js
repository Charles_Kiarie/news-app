import React from 'react'


import Styles from '../videolist.module.css'
import VideoListTemplate from '../videoListTemplate'

const VideosRelated = (props) => {
    return (
        <div className={Styles.relatedwrapper}>
            <VideoListTemplate data={props.data} teams={props.teams}/>
        </div>
    )
}

export default VideosRelated