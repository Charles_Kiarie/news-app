import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import axios from 'axios'
import {CSSTransition, TransitionGroup} from 'react-transition-group'
import { URL } from '../../../helper'
import styles from './newslist.module.css'
import Button from '../Button/button'
import CardInfo from '../CardInfo/cardinfo'

class NewsList extends Component {

    state = {
        items: [],
        teams: [],
        start: this.props.start,
        end: this.props.start + this.props.amount,
        amount: this.props.amount
    }

    componentWillMount() {
        this.request(this.state.start, this.state.end)
    }

    request = async (start, end) => {
        if(this.state.teams < 1) {
            let res = await axios.get(`${URL}/teams`)
            this.setState({
               teams: res.data
            })
        }

        let res = await axios.get(`${URL}/articles?_start=${start}&_end=${end}`)
        this.setState({
            items: [...this.state.items, ...res.data], 
            start,
            end
        })
    }

    loadMore = () => {
        let end = this.state.end + this.state.amount
        this.request(this.state.end, end)
    }

    renderNews = (type) => {
        let template = null

        switch (type) {
            case('card'):
                template = this.state.items.map((item, i) => {
                    return (
                        <CSSTransition
                            classNames={{
                                enter: styles.newslist_wrapper,
                                enterActive: styles.newslist_wrapper_enter
                            }}
                            timeout={500}
                            key={i}
                        >
                            <div>
                                <div className={styles.newslist_item}>
                                    <Link to={`/articles/${item.id}`}>
                                        <CardInfo teams={this.state.teams} teamId={item.team} date={item.date}/>
                                        <h2>{item.title}</h2>
                                    </Link>
                                </div>
                            </div>
                        </CSSTransition>
                    )
                })
            break;

            case('main'):
                template = this.state.items.map((item, i) => {
                    return (
                        <CSSTransition
                            classNames={{
                                enter: styles.newslist_wrapper,
                                enterActive: styles.newslist_wrapper_enter
                            }}
                            timeout={500}
                            key={i}
                        >
                            <Link to={`/articles/${item.id}`}>
                                <div className={styles.flex_wrapper}>
                                    <div className={styles.left}>
                                        style={{
                                            background: (`url('/images/articles/${item.image}')`)
                                        }}
                                        <div></div>
                                    </div>
                                    <div className={styles.right}>
                                        <CardInfo teams={this.state.teams} teamId={item.team} date={item.date}/>
                                        <h2>{item.title}</h2>
                                    </div>
                                </div>
                            </Link>
                        </CSSTransition>
                    )
                })
            break;
        
            default:
                template = null
        }

        return template;
    }


    render() {
        return(
            <div>
                <TransitionGroup
                    component="div"
                    className="list"
                >
                    {this.renderNews(this.props.type)}
                </TransitionGroup>
                <Button
                    type="loadmore"
                    loadMore={() => this.loadMore()}
                    text="Load More News"
                >
                </Button>
            </div>
        )
    }
}

export default NewsList