import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom';

import Home from './components/Home/home'
import Layout from './hoc/Layout/layout';
import NewsArticle from './components/Articles/News/index'
import VideoArticle from './components/Articles/Videos/index';
import NewsMain from './components/Articles/News/newsmain';
import VideosMain from './components/Articles/Videos/videosmain';

class Routes extends Component {

    state = {
        
    }

    render() {
        return (
            <Layout>
                <Switch>
                    <Route path="/" exact component={Home}/>
                    <Route path="/news" exact component={NewsMain}/>
                    <Route path="/articles/:id" exact component={NewsArticle}/>
                    <Route path="/videos/:id" exact component={VideoArticle}/>
                    <Route path="/videos" exact component={VideosMain}/>
                </Switch>
            </Layout>
            
        )
    }
}

export default Routes;