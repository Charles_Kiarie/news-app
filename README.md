# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

## Clone the repository

Use `git clone https://Charles_Kiarie@bitbucket.org/Charles_Kiarie/news-app.git`

### `npm install`

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run serve`

This script will run json-server with mock data from mock db.json file found in the root folder.
Open [http://localhost:3001](http://localhost:3001) to view it in the browser.

It exposes 3 endpoints:

`[http://localhost:3001/articles](http://localhost:3001/articles)`

`[http://localhost:3001/videos](http://localhost:3001/videos)`

`[http://localhost:3001/teams](http://localhost:3001/teams)`

## Technologies used:
- React js
- Axios
- React-router-dom, react-slick(carousel component), json-server
-React-sidenav(to implement the sidebar)

## Notes & Considerations

A refresher project just to have fun with react js. Used deprecated lifecycle methods and old style class-based components to manage state. Functional components never handled any state only rendered jsx components. Upcoming projects will be sure to use react hooks to make my components simpler and comply with modern react standards.

Had fun with it seeing how the framework has changed for the better over the years but now it is time to build a big boy react project. :)

## Screenshots

![App screenshots](https://drive.google.com/uc?export=view&id=1-YU7zeTIpqax1nPIwGPdOoEUt0Rd6A9y)